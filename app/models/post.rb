class Post < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    validates_presence_of :tittle  # a post won't save unless it has a title and a body.
    validates_presence_of :body # That's what these two lines do.
end
